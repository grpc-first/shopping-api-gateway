package services

import (
	"fmt"

	"gitlab.com/grpc-first/shopping-api-gateway/config"
	pay "gitlab.com/grpc-first/shopping-api-gateway/genproto/payment"
	ps "gitlab.com/grpc-first/shopping-api-gateway/genproto/product"

	ss "gitlab.com/grpc-first/shopping-api-gateway/genproto/store"
	us "gitlab.com/grpc-first/shopping-api-gateway/genproto/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/resolver"
)

type IServiceManager interface {
	UserService() us.UserServiceClient
	ProductService() ps.ProductServiceClient
	StoreService() ss.StoreServiceClient
	PaymentService() pay.PaymentServiceClient
}

type serviceManager struct {
	userService    us.UserServiceClient
	productService ps.ProductServiceClient
	storeService   ss.StoreServiceClient
	paymentService pay.PaymentServiceClient
}

func (s *serviceManager) UserService() us.UserServiceClient {
	return s.userService
}

func (s *serviceManager) ProductService() ps.ProductServiceClient {
	return s.productService
}

func (s *serviceManager) StoreService() ss.StoreServiceClient {
	return s.storeService
}

func (s *serviceManager) PaymentService() pay.PaymentServiceClient {
	return s.paymentService
}

func NewServiceManager(conf *config.Config) (IServiceManager, error) {
	resolver.SetDefaultScheme("dns")

	connUser, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.UserServiceHost, conf.UserServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	connPayment, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.PaymentServiceHost, conf.PaymentServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connProduct, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.ProductServiceHost, conf.ProductServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	connStore, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.StoreServiceHost, conf.StoreServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	serviceManager := &serviceManager{
		userService:    us.NewUserServiceClient(connUser),
		paymentService: pay.NewPaymentServiceClient(connPayment),
		productService: ps.NewProductServiceClient(connProduct),
		storeService:   ss.NewStoreServiceClient(connStore),
	}

	return serviceManager, nil
}
