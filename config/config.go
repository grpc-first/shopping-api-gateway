package config

import (
	"os"

	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment        string // develop, staging, production
	UserServiceHost    string
	UserServicePort    int
	ProductServiceHost string
	ProductServicePort int
	StoreServiceHost   string
	StoreServicePort   int
	PaymentServiceHost string
	PaymentServicePort int
	CtxTimeout         int // context timeout in seconds
	LogLevel           string
	HTTPPort           string
	RedisHost          string
	RedisPort          string
}

// Load loads environment vars and inflates Config
func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))

	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8080"))

	c.UserServiceHost = cast.ToString(getOrReturnDefault("USER_SERVICE_HOST", "127.0.0.1"))
	c.UserServicePort = cast.ToInt(getOrReturnDefault("USER_SERVICE_PORT", 8000))

	c.ProductServiceHost = cast.ToString(getOrReturnDefault("PRODUCT_SERVICE_HOST", "127.0.0.1"))
	c.ProductServicePort = cast.ToInt(getOrReturnDefault("PRODUCT_SERVICE_PORT", 9000))

	c.StoreServiceHost = cast.ToString(getOrReturnDefault("STORE_SERVICE_HOST", "127.0.0.1"))
	c.StoreServicePort = cast.ToInt(getOrReturnDefault("STORE_SERVICE_PORT", 6000))

	c.PaymentServiceHost = cast.ToString(getOrReturnDefault("PAYMENT_SERVICE_HOST", "127.0.0.1"))
	c.PaymentServicePort = cast.ToInt(getOrReturnDefault("PAYMENT_SERVICE_PORT", 7000))

	c.RedisHost = cast.ToString(getOrReturnDefault("REDIS_HOST", "localhost"))
	c.RedisPort = cast.ToString(getOrReturnDefault("REDIS_PORT", "6379"))

	c.CtxTimeout = cast.ToInt(getOrReturnDefault("CTX_TIMEOUT", 7))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
