package v1

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"gitlab.com/grpc-first/shopping-api-gateway/genproto/user"
	"gitlab.com/grpc-first/shopping-api-gateway/pkg/etc"
	"gitlab.com/grpc-first/shopping-api-gateway/pkg/logger"
	"google.golang.org/protobuf/encoding/protojson"
)

// Register part is not fully ready


// Register user
// @Summary      Register user
// @Description  Registers user
// @Tags         User
// @Accept       json
// @Produce      json
// @Param        user   body   user.User     true  "User"
// @Success      200  {object}  user.User
// @Router       /register [post]
func (h *handlerV1) RegisterUser(c *gin.Context) {
	var body user.User

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"Error": err.Error(),
			"Hint":  "Check your data",
		})
		h.log.Error("Error while binding json", logger.Any("json", err))
		return
	}
	body.Email = strings.TrimSpace(body.Email)
	body.Usenrame = strings.TrimSpace(body.Usenrame)

	body.Email = strings.ToLower(body.Email)

	body.Password, err = etc.HashPassword(body.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "Something went wrong")
		h.log.Error("couldn't hash the password")
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	emailExists, err := h.serviceManager.UserService().CheckField(ctx, &user.CheckFieldReq{
		Field: "email",
		Value: body.Email,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		h.log.Error("Error while cheking email uniqeness", logger.Any("check", err))
		return
	}

	if emailExists.Exist {
		c.JSON(http.StatusConflict, gin.H{
			"info": "Email is already used",
		})
		return
	}

	usernameExists, err := h.serviceManager.UserService().CheckField(ctx, &user.CheckFieldReq{
		Field: "username",
		Value: body.Usenrame,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		h.log.Error("Error while cheking username uniqeness", logger.Any("check", err))
		return
	}

	if usernameExists.Exist {
		c.JSON(http.StatusConflict, gin.H{
			"info": "Username is already used",
		})
		return
	}

	c.JSON(http.StatusAccepted, gin.H{
		"info": "Your request is accepted we have sent you an email message, please check and verify",
	})

	body.Code = etc.GenerateCode(6)

	bodyByte, err := json.Marshal(body)
	if err != nil {
		h.log.Error("Error while marshaling to json", logger.Any("json", err))
		return
	}
	err = h.redis.SetWithTTL(body.Email, string(bodyByte), 300)

	if err != nil {
		h.log.Error("Error while marshaling to json", logger.Any("json", err))
		return
	}
}

// Verify user
// @Summary      Verify user
// @Description  Verifys user
// @Tags         User
// @Accept       json
// @Produce      json
// @Param        code   path string true "code"
// @Param        email  path string true "email"
// @Success      200  {object}  user.User
// @Router       /verify/{email}/{code} [patch]
func (h *handlerV1) Verify(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	var (
		code  = c.Param("code")
		email = c.Param("email")
	)

	userBody, err := h.redis.Get(email)
	if err != nil {
		c.JSON(http.StatusGatewayTimeout, gin.H{
			"info":  "Your time has expired",
			"error": err.Error(),
		})
		h.log.Error("Error while getting user from redis", logger.Any("redis", err))
	}
	userBodys := cast.ToString(userBody)
	body := user.User{}
	err = json.Unmarshal([]byte(userBodys), &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while unmarshaling from json to user body", logger.Any("json", err))
		return
	}
	if body.Code != code {
		c.JSON(http.StatusConflict, gin.H{
			"info": "Wrong code",
		})
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	res, err := h.serviceManager.UserService().CreateUser(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while creating user", logger.Any("post", err))
		return
	}
	c.JSON(http.StatusOK, res)
}
