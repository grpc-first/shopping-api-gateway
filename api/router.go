package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/grpc-first/shopping-api-gateway/api/docs" //swag
		v1 "gitlab.com/grpc-first/shopping-api-gateway/api/handlers/v1"
	"gitlab.com/grpc-first/shopping-api-gateway/config"
	"gitlab.com/grpc-first/shopping-api-gateway/pkg/logger"
	"gitlab.com/grpc-first/shopping-api-gateway/services"
	"gitlab.com/grpc-first/shopping-api-gateway/storage/repo"
)

// Option ...
type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Redis          repo.InMemoryStorageI
}

// New ...
// @title           Shopping api
// @version         1.0
// @description     This is shopping server api server
// @termsOfService  not much usefull

// @contact.name   Azizbek
// @contact.url    https://t.me/azizbek_dev_2005
// @contact.email  azizbekhojimurodov@gmail.com

// @host      localhost:8080
// @BasePath  /v1

// @securityDefinitions.basic  BasicAuth
func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
		Redis:          option.Redis,
	})

	api := router.Group("/v1")

	// User
	api.POST("/users", handlerV1.CreateUser)
	api.GET("/users/:id", handlerV1.GetUserInfo)
	api.DELETE("/users/:id", handlerV1.DeleteUser)
	api.PUT("/users", handlerV1.UpdateUser)
	api.PATCH("/users/buy", handlerV1.BuyProduct)

	// Payment...
	api.PUT("/card/add", handlerV1.AddBalance)

	// Store...
	api.POST("/stores", handlerV1.CreateStore)
	api.GET("/stores/:id", handlerV1.GetStoreInfo)

	// Product
	api.POST("/products", handlerV1.CreateProduct)
	api.GET("/products", handlerV1.GetProductsByIds)

	// profile
	api.POST("/profile/:id", handlerV1.SetProfilePhoto)

	// register
	api.POST("/register", handlerV1.RegisterUser)
	api.PATCH("/verify/:email/:code", handlerV1.Verify)

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
	return router
}
