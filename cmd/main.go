package main

import (
	"github.com/gomodule/redigo/redis"
	"gitlab.com/grpc-first/shopping-api-gateway/api"
	"gitlab.com/grpc-first/shopping-api-gateway/config"
	"gitlab.com/grpc-first/shopping-api-gateway/pkg/logger"
	"gitlab.com/grpc-first/shopping-api-gateway/services"
	r "gitlab.com/grpc-first/shopping-api-gateway/storage/redis"
)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "api_gateway")

	serviceManager, err := services.NewServiceManager(&cfg)
	if err != nil {
		log.Error("gRPC dial error", logger.Error(err))
	}
	pool := &redis.Pool{
		MaxIdle: 10,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", cfg.RedisHost+":"+cfg.RedisPort)
		},
	}

	server := api.New(api.Option{
		Conf:           cfg,
		Logger:         log,
		ServiceManager: serviceManager,
		Redis:          r.NewRedisRepo(pool),
	})
	if err := server.Run(cfg.HTTPPort); err != nil {
		log.Fatal("failed to run http server", logger.Error(err))
		panic(err)
	}
}
