package v1

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	pay "gitlab.com/grpc-first/shopping-api-gateway/genproto/payment"
	us "gitlab.com/grpc-first/shopping-api-gateway/genproto/user"

	l "gitlab.com/grpc-first/shopping-api-gateway/pkg/logger"
	"google.golang.org/protobuf/encoding/protojson"
)
// Create user
// @Summary      Create user
// @Description  Creates new user
// @Tags         User
// @Accept       json
// @Produce      json
// @Param        user   body   user.User     true  "User"
// @Success      200  {object}  user.User
// @Router       /users [post]
func (h *handlerV1) CreateUser(c *gin.Context) {
	var (
		body        us.User
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.UserService().CreateUser(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		h.log.Error("Failed to create user", l.Error(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}

// Get user info
// @Summary      Get user info
// @Description  Get user info with its productas 
// @Tags         User
// @Accept       json
// @Produce      json
// @Param        id   path int true "id"
// @Success      200  {object}  user.UserInfo
// @Router       /users/{id} [get]
func (h *handlerV1) GetUserInfo(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.UserService().GetUserInfoById(ctx, &us.Id{Id: id})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get user info", l.Error(err))
		return
	}
	
	c.JSON(http.StatusOK, response)
}

// Add balanace
// @Summary      Add balanace
// @Description  Add balanace to specific owner's card 
// @Tags         Payment
// @Accept       json
// @Produce      json
// @Param        user   body   payment.UpdateBalanceRequest  true  "update balance"
// @Success      200  {object}  user.Card
// @Router       /card/add [put]
func (h *handlerV1) AddBalance(c *gin.Context) {
	var (
		body        pay.UpdateBalanceRequest
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("coudn't bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.PaymentService().AddBalance(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error while adding balance to card", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// Delete user
// @Summary      Delete user
// @Description  Delete user with id 
// @Tags         User
// @Accept       json
// @Produce      json
// @Param        id   path int true "id"
// @Success      200  {object}  user.Empty
// @Router       /users/{id} [delete]
func (h *handlerV1) DeleteUser(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	uid := c.Param("id")
	id, err := strconv.ParseInt(uid, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"Error": err.Error(),
		})
		h.log.Error("Error while parsing uid to int id", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	_, err = h.serviceManager.UserService().DeleteUser(ctx, &us.Ids{Ids: []int64{id}})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		h.log.Error("Error while deleting user", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"Status": "User succesfully deleted",
	})
}

// Update user
// @Summary      Update user
// @Description  Updates user
// @Tags         User
// @Accept       json
// @Produce      json
// @Param        user   body   user.User     true  "User"
// @Success      200  {object}  user.User
// @Router       /users [put]
func (h *handlerV1) UpdateUser(c *gin.Context) {
	var (
		body        us.User
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.UserService().UpdateUser(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		h.log.Error("Failed to Update user", l.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// Buy product
// @Summary      Buy product
// @Description  Buy product to user
// @Tags         User
// @Accept       json
// @Produce      json
// @Param        user   body   user.BuyProductRequest     true  "User"
// @Success      200  {object}  user.BuyProductResponse
// @Router       /users/buy [patch]
func (h *handlerV1) BuyProduct(c *gin.Context) {
	var (
		body        us.BuyProductRequest
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.UserService().BuyProduct(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		h.log.Error("Failed to buy product", l.Error(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}