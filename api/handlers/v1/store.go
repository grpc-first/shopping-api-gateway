package v1

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	ss "gitlab.com/grpc-first/shopping-api-gateway/genproto/store"
	l "gitlab.com/grpc-first/shopping-api-gateway/pkg/logger"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create store
// @Summary      Create store
// @Description  Creates new store
// @Tags         stores
// @Accept       json
// @Produce      json
// @Param        store   body   store.Store     true  "Store"
// @Success      200  {object}  store.Store
// @Router       /stores [post]
func (h *handlerV1) CreateStore(c *gin.Context) {
	var (
		body        ss.Store
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.StoreService().CreateStore(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		h.log.Error("Failed to create store", l.Error(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}

// Get Store info
// @Summary      Get store info
// @Description  Get store info with its products and address
// @Tags         stores
// @Accept       json
// @Produce      json
// @Param        id   path int true "id"
// @Success      200  {object}  store.Store
// @Router       /users/{id} [get]
func (h *handlerV1) GetStoreInfo(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.StoreService().GetStoreProductsByIds(ctx, &ss.Ids{Ids: []int64{id}})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get store info", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}


