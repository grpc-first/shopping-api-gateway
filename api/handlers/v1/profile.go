package v1

import (
	"context"
	"encoding/base64"
	"fmt"
	"image"
	"image/jpeg"
	"log"
	"net/http"
	"os"	
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/grpc-first/shopping-api-gateway/genproto/user"
	"gitlab.com/grpc-first/shopping-api-gateway/pkg/logger"
	"google.golang.org/protobuf/encoding/protojson"
)

//// Set profile photo
//// Summary      Profile image
//// Description  set profile image
//// Tags         User
//// Accept       json
//// Produce      file
//// Param        id  path   int   true  "id"
//// Param        photo body multipart.FileHeader true "image"
//// Success      200  {object}  file
//// Router       /profile/{id} [post]
func (h *handlerV1) SetProfilePhoto(c *gin.Context) {
	var (
		body        user.SetProfileImageReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	file, err := c.FormFile("photo")
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "No file is received",
		})
		return
	}

	isS := c.Param("id")
	idint, err := strconv.ParseInt(isS, 10, 64)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "NO id parametr",
		})
		return
	}
	body.OwnerId = idint

	extension := filepath.Ext(file.Filename)
	newFileName := "image" + isS + extension

	if err := c.SaveUploadedFile(file, "./"+newFileName); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"message": "Unable to set profile photo",
			"Error":   err.Error(),
		})
		return
	}

	bts, err := os.ReadFile("./" + newFileName)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "couln't set photo")
		h.log.Error("Error while reading file", logger.Any(newFileName, err))
		return
	}
	err = os.Remove("./" + newFileName)
	if err != nil {
		h.log.Error("Error while deleting photo", logger.Any("Delete", err))
	}
	mimeType := http.DetectContentType(bts)
	switch mimeType {
	case "image/jpeg":
		body.TypeInfo = "data:image/jpeg;base64"
	case "image/png":
		body.TypeInfo = "data:image/png;base64"
	case "image/jpg":
		body.TypeInfo = "data:image/jpg;base64"
	}
	body.Int64S = ToBase64(bts)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.UserService().SetProfileImage(ctx, &body)
	if err != nil {
		fmt.Println("Err")
		c.JSON(http.StatusInternalServerError, "Coulnd't set profile image")
		h.log.Error("Error while setting profile image", logger.Any("post", err))
		return
	}

	filename, err := Base64toJpg(response.Int64S, newFileName)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "Coulnd't set profile image")
		h.log.Error("Error while setting profile image", logger.Any("post", err))
		return
	}
	c.Status(http.StatusOK)
	c.File(filename)
	os.Remove(filename)

}

func ToBase64(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}

func Base64toJpg(data, fileName string) (string, error) {

	reader := base64.NewDecoder(base64.StdEncoding, strings.NewReader(data))
	m, formatString, err := image.Decode(reader)
	if err != nil {
		log.Fatal(err)
	}
	bounds := m.Bounds()
	fmt.Println("base64toJpg", bounds, formatString)

	//Encode from image format to writer

	f, err := os.OpenFile(fileName, os.O_WRONLY|os.O_CREATE, 0777)
	if err != nil {
		log.Fatal(err)
		return "", err
	}

	err = jpeg.Encode(f, m, &jpeg.Options{Quality: 75})
	if err != nil {
		log.Fatal(err)
		return "", err
	}
	return fileName, nil
}
