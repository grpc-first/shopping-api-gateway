package v1

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/grpc-first/shopping-api-gateway/genproto/product"
	"gitlab.com/grpc-first/shopping-api-gateway/pkg/logger"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create product
// @Summary 		Create product
// @Description 	this creates product
// @Tags 			Product
// @Accept 			json
// @Produce         json
// @Param           product  			  body  	product.Product true "Product"
// @Success         200					  {object} 	product.Product
// @Router          /products [post]
func (h *handlerV1) CreateProduct(c *gin.Context) {
	var (
		body        product.Product
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while binding json", logger.Any("json", err))
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ProductService().CreateProduct(ctx, &body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while creating product info", logger.Any("create", err))
	}
	c.JSON(http.StatusCreated, response)
}

// Get products by ids
// @Summary Get products
// @Description Gets products by ids
// @Tags Product
// @Accept json
// @Produce json
// @Param        id   query []int true "product ids"
// @Success      200  {object}  product.GetProducts
// @Router       /products [get]
func (h *handlerV1) GetProductsByIds(c *gin.Context) {
	var (
		body        product.GetProductsByIdsRequest
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	ids := c.QueryArray("id")

	for _, id := range ids {
		idint, err := strconv.ParseInt(id, 10, 64)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			h.log.Error("Error while converting to int", logger.Any("convert", err))
		}
		body.Ids = append(body.Ids, idint)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	products, err := h.serviceManager.ProductService().GetProductsByIds(ctx, &body)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"error": err.Error(),
			"hint":  "Not Found",
		})
		h.log.Error("Error while getting products", logger.Any("json", err))
	}

	c.JSON(http.StatusOK, products)
}
