package v1

import (
	"gitlab.com/grpc-first/shopping-api-gateway/config"
	"gitlab.com/grpc-first/shopping-api-gateway/pkg/logger"
	"gitlab.com/grpc-first/shopping-api-gateway/services"
	"gitlab.com/grpc-first/shopping-api-gateway/storage/repo"
)

type handlerV1 struct {
	log            logger.Logger
	serviceManager services.IServiceManager
	cfg            config.Config
	redis          repo.InMemoryStorageI
}

// HandlerV1Config ...
type HandlerV1Config struct {
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Cfg            config.Config
	Redis          repo.InMemoryStorageI
}

// New ...
func New(c *HandlerV1Config) *handlerV1 {
	return &handlerV1{
		log:            c.Logger,
		serviceManager: c.ServiceManager,
		cfg:            c.Cfg,
		redis:          c.Redis,
	}
}
